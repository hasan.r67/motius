from rest_framework import serializers
from my_serializer import MyArticleSerializer

from field_permission.models import Article

class ArticleSerializer(MyArticleSerializer):
    class Meta:
        model = Article
        fields = [
            "name",
            "author",
        ]
