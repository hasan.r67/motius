from django.apps import AppConfig


class FieldPermissionConfig(AppConfig):
    name = 'field_permission'
