from django.db import models

class Article(models.Model):
    name = models.CharField(max_length=100)
    author = models.CharField(max_length=100)
    class Meta:
        permissions = (
            ("read_name_article", "Can read article name"),
            ("change_name_article", "Can change article name"),
            ("read_article", "Can read article"),
        )

    def __str__(self):
        return self.name
