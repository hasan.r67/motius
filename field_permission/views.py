from rest_framework import serializers, viewsets, routers
from rest_framework.permissions import IsAuthenticated

from field_permission.serializers import ArticleSerializer
from field_permission.models import Article
from field_permission.permissions import ArticlePermission

class ArticleViewSet(viewsets.ModelViewSet):
    permission_classes =  (IsAuthenticated, ArticlePermission)
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer

router = routers.DefaultRouter()
router.register(r'article', ArticleViewSet)
