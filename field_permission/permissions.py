from rest_framework import permissions

action_mapping = {
	'list': 'field_permission.read_article',
	'create': 'field_permission.add_article',
	'update': 'field_permission.change_article',
	'destroy': 'field_permission.delete_article',
}
class ArticlePermission(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.user.is_superuser:
            return True
        action_perm = action_mapping.get(view.action, None)
        if action_perm is None:
            return True
        if request.user.has_perm(action_perm):
            return True
        return False
