from rest_framework.serializers import ModelSerializer
from django.contrib.auth.models import Permission
from collections import OrderedDict


class MyArticleSerializer(ModelSerializer):
    def to_representation(self, instance):
        all_perms = [x.codename for x in Permission.objects.all()]
        result = OrderedDict()
        ret = super(ModelSerializer, self).to_representation(instance)
        user = self.context['request'].user
        for field_name, field_value in ret.items():
            perm_name = 'read_%s_article' % field_name
            if perm_name in all_perms:
                if user.has_perm('field_permission.%s'%perm_name):
                    result[field_name] = field_value
            else:
                result[field_name] = field_value
        return result

    def to_internal_value(self, data):
        all_perms = [x.codename for x in Permission.objects.all()]
        result = OrderedDict()
        ret = super(ModelSerializer, self).to_internal_value(data)
        user = self.context['request'].user
        for field_name, field_value in ret.items():
            perm_name = 'read_%s_article' % field_name
            if perm_name in all_perms:
                if user.has_perm('field_permission.%s'%perm_name):
                    result[field_name] = field_value
            else:
                result[field_name] = field_value
        return result
